#!/usr/bin/env bash

SCRIPT=`realpath $0`
SCRIPT_HOME=`dirname $SCRIPT`

APLAYER="/tmp/aplayer"
PLAYLIST="/tmp/aplayer.playlist"

function toUpper {
  echo $@ | tr '[:lower:]' '[:upper:]'
}

function parse_args {
  COMM=`toUpper $1`
  AUX=$@
  offset=$((${#COMM} + 1))
  length=$((${#AUX} - offset))
  ARGS="${AUX:offset:length}"

  case "$COMM" in
    PAUSE) pause
      ;;
    NEXT) next
      ;;
    PREV) prev
      ;;
    DAEMON) _daemon
      ;;
    KILL) _kill
      ;;
    ADD) add "$ARGS"
      ;;
    LIST) list
      ;;
    *) usage
      ;;
  esac
}

function pause {
  echo "PAUSE" >> "$APLAYER"
}

function next {
  echo "NEXT" >> "$APLAYER"
}

function prev {
  echo "PREV" >> "$APLAYER"
}

function _daemon {
  ${SCRIPT_HOME}/daemon & >> "${SCRIPT_HOME}/log"
}

function _kill {
  echo "KILL" >> "$APLAYER"
}

function add {
  echo "$@" >> "$APLAYER"
}

function list {
  cat "$PLAYLIST"
}

function usage {
  echo "usage: aplayer <COMMAND> [ARGS]"
  echo -e "\nCOMMAND:"
  # echo -e "\n    add <PATH>    add the content of PATH at the end of the current playlist. if PATH is a directory then it find all the .mp3 files and then add each one to the current playlist. if PATH is a file, it just add that file."
  echo -e "\n    add <PATH>    if PATH is a file, then add that file to the current playlist (at the end). otherwise if PATH is a directory then add all .mp3 files of that path (all subdirectories will be scanned)."
  echo -e "\n    pause         play / pause current song."
  echo -e "\n    next          play next song of the playlist."
  echo -e "\n    prev          play prev song of the playlist."
  echo -e "\n    list          show song filepaths in the current playlist."
  echo -e "\n    daemon        starts aplayer daemon."
  echo -e "\n    kill          kill aplayer daemon."
}

parse_args "$@"
