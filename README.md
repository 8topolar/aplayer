# aplayer

music daemon player using fifo files and [mpg123](https://mpg123.de).

## usage

```
aplayer-cli <COMMAND> [ARGS]

COMMAND:

    add <PATH>    if PATH is a file, then add that file to the current playlist (at the end). otherwise if PATH is a directory then add all .mp3 files of that path (all subdirectories will be scanned).

    pause         play / pause current song.

    next          play next song of the playlist.

    prev          play prev song of the playlist.

    playlist      show song filepaths in the current playlist.

    daemon        starts aplayer daemon.

    kill          kill aplayer daemon.
```
